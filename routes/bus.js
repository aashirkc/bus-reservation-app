const { Bus, validate } = require("../model/bus");
const express = require("express");
const router = express.Router();
const multer = require("multer");

const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/", async (req, res) => {
  const bus = await Bus.find();
  res.send(bus);
});

router.post("/", upload.single("image"), async (req, res) => {
  console.log(req.file);
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let bus = new Bus({
    busNo: req.body.busNo,
    busType: req.body.busType,
    noOfSeats: req.body.noOfSeats,
    noOfRows: req.body.noOfRows,
    noOfColumns: req.body.noOfColumns,
    isAvailable: req.body.isAvailable,
    image: req.file.path
  });

  bus = await bus.save();
  res.send(bus);
});

router.put("/:id", upload.single("image"), async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const bus = await Bus.findByIdAndUpdate(
    req.params.id,
    {
      busNo: req.body.busNo,
      busType: req.body.busType,
      noOfSeats: req.body.noOfSeats,
      noOfRows: req.body.noOfRows,
      noOfColumns: req.body.noOfColumns,
      isAvailable: req.body.isAvailable,
      image: req.file.path
    },
    { new: true }
  );

  if (!bus) res.status(400).send("The bus with given id is not found");
  res.send(bus);
});

router.delete("/:id", async (req, res) => {
  const bus = await Bus.findByIdAndRemove(req.params.id);

  if (!bus)
    return res.status(404).send("The bus with the given ID was not found.");

  res.send(bus);
});

module.exports = router;
