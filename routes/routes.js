const express = require("express");
const router = express.Router();
const { Route, validate } = require("../model/routes");

router.get("/", async (req, res) => {
  const route = await Route.find().populate("busId");
  res.send(route);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let route = new Route({
    from: req.body.from,
    to: req.body.to,
    fromTime: req.body.fromTime,
    toTime: req.body.toTime,
    date: req.body.date,
    busId: req.body.busId
  });
  route = await route.save();
  res.send(route);
});

router.put("/:id", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const route = await Route.findByIdAndUpdate(
    req.params.id,
    {
      from: req.body.from,
      to: req.body.to,
      fromTime: req.body.fromTime,
      toTime: req.body.toTime,
      date: req.body.date,
      busId: req.body.busId
    },
    { new: true }
  );

  if (!route) res.status(400).send("The route with given id is not found");
  res.send(route);
});

router.delete("/:id", async (req, res) => {
  const route = await Route.findByIdAndRemove(req.params.id);

  if (!route)
    return res.status(404).send("The route with the given ID was not found.");

  res.send(route);
});

module.exports = router;
