const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");

const bus = require("./routes/bus");

const routes = require("./routes/routes");
const mongoose = require("mongoose");
const app = express();

mongoose
  .connect("mongodb://localhost/bus-reservation", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log("Connected to Mongodb..."))
  .catch(err => console.error("could not connect to MongoDb"));

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb" }));
app.use("/api/bus", bus);
app.use("/uploads", express.static("uploads"));
app.use("/api/routes", routes);

app.use(helmet());

app.listen(3200, () => console.log("Listening to port"));
