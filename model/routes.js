const mongoose = require("mongoose");
const joi = require("joi");
const busSchema = require("./bus");

const route = new mongoose.Schema({
  from: {
    type: String,
    required: true
  },
  to: {
    type: String,
    required: true
  },
  fromTime: {
    type: String
  },
  toTime: {
    type: String
  },
  date: {
    type: String,
    required: true
  },
  busId: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Bus"
    }
  ]
});

function validateRoute(route) {
  const schema = {
    from: joi.string().required(),
    to: joi.string().required(),
    fromTime: joi.allow(),
    toTime: joi.allow(),
    date: joi.allow(),
    busId: joi.allow()
  };

  return joi.validate(route, schema);
}

const Route = mongoose.model("Route", route);

exports.Route = Route;
exports.validate = validateRoute;
