const joi = require("joi");
const mongoose = require("mongoose");

const busSchema = new mongoose.Schema({
  busNo: {
    type: String,
    required: true,
    maxlength: 50
  },
  busType: {
    type: String,
    required: true,
    maxlength: 40
  },
  noOfSeats: {
    type: Number,
    required: true
  },
  noOfRows: {
    type: Number
  },
  noOfColumns: {
    type: Number
  },
  isAvailable: {
    type: Boolean,

    default: true
  },
  image: {
    type: String
  }
});

const Bus = mongoose.model("Bus", busSchema);

function validateBus(bus) {
  const schema = {
    busNo: joi.string().required(),
    busType: joi.string().required(),
    noOfSeats: joi.allow(),
    noOfRows: joi.allow(),
    noOfColumns: joi.allow(),
    isAvailable: joi.allow(),
    image: joi.allow()
  };

  return joi.validate(bus, schema);
}

exports.Bus = Bus;
exports.busSchema = busSchema;
exports.validate = validateBus;
